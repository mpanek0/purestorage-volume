"""
Plug-in for creating a Pure Storage volume.
"""
from __future__ import unicode_literals
from infrastructure.models import CustomField, Environment
from common.methods import set_progress
from utilities.models import ConnectionInfo
import purestorage
import datetime
import time


def run(job, *args, **kwargs):
    volume_name = '{{ volume_name }}'
    volume_size = '{{ volume_size }}'

    ci=ConnectionInfo.objects.filter(name__iexact='Pure Storage array').first()

    '''
    #Fake
    array=purestorage.FlashArray(ci.ip,ci.username,ci.password)
    #establish session
    array.get()
    '''
    time.sleep(2)
    set_progress('Flash array session established')

    CustomField.objects.get_or_create(
        name='size',
        defaults= {
            "label": 'Pure storage size', "type": 'STR',
            "description": 'Used by the pure storage blueprint'
        }
    )

    CustomField.objects.get_or_create(
        name='created',
        defaults= {
            "label":'Pure Storage volume created date',
            "type": 'STR',
            "description": 'Used by the Pure storage blueprint'
        }
    )
    CustomField.objects.get_or_create(
        name='serial',
        defaults= {
            "label":'Pure Storage volume id',
            "type": 'STR',
            "description": 'Used by the Pure storage blueprint'
        }
    )

    resource = kwargs.pop('resources').first()
    resource.name = volume_name
    resource.size = volume_size
    resource.serial='DABA29111570F7A4000114C1'
    resource.created = datetime.datetime.now()
    resource.save()

    set_progress("creating storage volume %s" % volume_name)
    time.sleep(2)
    
    set_progress('Created storage volume: "%s"' % volume_name)

    return "SUCCESS", "", ""